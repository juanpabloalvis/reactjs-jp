"Proyecto React Js version 16, babel 16.15" 

```console
reactjs-jp>node --version
v8.10.0
reactjs-jp>npm --version
5.6.0
```

#### Instalar comando que crea app de react:
```console
npm install -g create-react-app
```
#### Crear app de react en ex-2:
```console
create-react-app bulletin-board-jp
cd bulletin-board-jp
```
#### Instalar iconos que vienen con react
```console
npm install --save react-icons
```
###Ejecutar como servicio 
Este proyecto tambien se puede utilizar como un serividor estatico, como un servicio, o un servicio http.
Para esto se debe instalar el servicio "serve" con el siguiente comando:
```console
jp\jp-bulletin-board>$ npm install -g serve
```
#### despues de instalado, llamamos:
```console
jp\jp-bulletin-board>$ serve -s build
```



### .:React Essential Course:. 
Es importante porque no necesitamos crear más paquetes, react-create-app hace todo el trabajo. (-g significa que es instalacion global)
```console
$ npm install -g create-react-app
```

En el folder que queremos crear el proyecto  
```console
$ create-react-app react-essential-1
npm start
```
El anterior comando abre un navegador, en la siguiente ruta : http://localhost:3000/ 

Los modulos estan compuestos de:

Proyecto  | Descripcion
------------- | -------------
react-essential-1  | Las bases
react-essential-2  | Crear componentes, llamado entre componentes
react-essential-3  | Crear funciones, componer componentes, estados, eventos, tipos de datos, json, rest
react-essential-4  | Modularizacion de la app, build

Despues que terminamos el desarrollo, se construye el proyecto, que es una versión minimizada y reducida lista para correre en producciòn, esto se hace con:
```console
$ npm run build
```
Y la podemos desplegar en un servidor local(instalarmo el servidor en caso que no lo hayamos instalado antes):
```console
$ sudo npm install serve -g
```
y desplegamos:
```console
$ serve -s build
```

Proximos pasos:
- React Receipes (Routes)
- React Lifecycles
- React Components, Context, and Accessibility