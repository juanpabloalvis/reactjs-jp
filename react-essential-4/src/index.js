import React, {Component} from 'react'; // Import the react library
import ReactDOM from 'react-dom'; // Import the react dom
import './index.css'; //
import * as serviceWorker from './serviceWorker';
import Library from './Library'

let bookList = [
    {"title": "El inversionista inteligente", "author": "Benjamin Graham", "pages": 792},
    {"title": "El cisne negro ", "author": "Nassim Nicholas Taleb", "pages": 592},
    {"title": "Scala for data science", "pages": 416}
]








class FavoriteColorForm extends React.Component {
    state = {value: ""}
    // e means event
    newColor = e => this.setState({value: e.target.value})
    submit = e => {
        console.log(`Nuevo color:  ${this.state.value}`)
        e.preventDefault() //evita enviar el formulario
    }

    render() {
        return (
            <form onSubmit={this.submit}>
                <label>Favorite Color(find in console.log):
                    <input type="color" onChange={this.newColor}/>
                </label>
                <button>Submit</button>
            </form>
        )
    }
}



ReactDOM.render(
    <div>

        {/* Podemos usar los valores por defecto así, <Library /> o proveer los valores*/}
        <Library books={bookList}/>
        <FavoriteColorForm/>
    </div>
    , document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
