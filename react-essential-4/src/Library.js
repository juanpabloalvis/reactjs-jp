import React, {Component} from 'react'; // Import the react library
import './index.css'; //
import PropTypes from 'prop-types'
import {Book} from './Book'
import {Hiring} from './Hiring'
import {NotHiring} from './NotHiring'


class Library extends Component {
    static defaultProps = {
        books: [
            {"title": "Scala for data science", "author": "Pascal Bugnion", "pages": 416}
        ]
    }
    state = {
        open: false,
        freeBookmark: true,
        hiring: false,
        data: [],
        loading: false
    }

    componentDidMount() {
        console.log("El componente fue montado")
        this.setState({loading: true})
        fetch('https://hplussport.com/api/products/order/price/sort/asc/qty/3')
            .then(data => data.json()) // la cambiamos a formato json
            .then(data => this.setState({data, loading: false})) //seteamos nuestro data con el valor que vienes desde la url
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("El componente fue actualizado")
    }

    toggleOpenClosed = () => {
        this.setState(previousState => ({
            open: !previousState.open
        }))
    }

    render() {
        console.log(this.state)
        // cuando definimos {books} hacemos que la propiedad props.book se enlace al {books}
        const {books} = this.props
        return (
            <div> Se debe utilizar un 'key' para identificar el recurso al que se accede
                <h1>La librería está {this.state.open ? 'abierta' : 'cerrada'}</h1>
                <button onClick={this.toggleOpenClosed}>Cambiar el estado(Importante)</button>
                {books.map(
                    (book, i) =>
                        <Book key={i}
                              title={book.title}
                              author={book.author}
                              pages={book.pages}
                            //Enviamos este estado al hijo, via props
                              freeBookmark={this.state.freeBookmark}/>
                )}
                {this.state.hiring ? <Hiring/> : <NotHiring/>}
                {
                    this.state.loading
                        ? "cargando..."
                        : <div>
                            <h3>Productos de la semana en la tienda</h3>
                            {this.state.data.map((product, i) => {
                                return (
                                    <div key={i}>
                                        <h2>{product.name}</h2>
                                        <img src={product.image} height={200} alt={product.name}/>
                                    </div>)
                            })}
                        </div>}
            </div>

        );
    }
}

// PropTypes permite que los datos que recibe cada componente, sean 'Tipados'
Library.propTypes = {
    // si no hay otro valor deberia haber un error
    books: PropTypes.array
}

export default Library