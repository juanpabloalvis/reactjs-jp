import React from 'react'; // Import the react library
import PropTypes from 'prop-types'
// Podemos poner valores por defecto al crear el componente
export const Book = ({title = "Sin título", author = "Sin author", pages = 0, freeBookmark}) => {
    return (
        <section>
            <h2>{title}</h2>
            <p>{author}</p>
            <p>Pages: {pages}</p>
            <p>Free Bookmark today: {freeBookmark ? 'yes' : 'no!'}</p>
        </section>)
}

Book.propTypes = {
    title: PropTypes.string,
    author: PropTypes.string,
    pages: PropTypes.number,
    freeBookmark: PropTypes.bool
}