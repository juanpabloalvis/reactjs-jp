import React, {Component} from 'react'; // Import the react library
import ReactDOM from 'react-dom'; // Import the react dom
import './index.css'; //
import * as serviceWorker from './serviceWorker';

let style = {
    backgroundColor: 'orange',
    color: 'gray',
    fontFamily: 'Arial'
}

let cyclingData = {
    dia: 12,
    distancia: 94.7,
    maxHr: 158,
    cadenciaMedia: 81,
    calorias: 1479
}

// Debe inicar con letra capital
class Message extends  Component {
    // Describe lo que vamos a presnetar en el DOM
    render() {
        // debe retornar un Html
        // Podemos poner las propiedades que queramos, en este caso las imprimimos
        console.log(this.props)
        return (
            <div>
                <h1>Hello every one </h1>
                <h2 style={{color: this.props.color}}>{this.props.msg}</h2>
                <div style={style}>
                    <p>Este es un parrafo con estilo </p>
                </div>
                <p>Regresare en {this.props.minutes} minutos.</p>
            </div>
        )
    }
}

class CyclingDaySummary extends Component {
    render() {
        console.log(this.props)
        return (
            <section>
                <div style={style}>Cycling day</div>
                <div><p>Dia: {this.props.dia}</p></div>
                <div><p>Distancia: {this.props.distancia}</p></div>
                <div><p>Frecuencia cardiaca máxima: {this.props.maxHr}</p></div>
                <div><p>Cadencia media: {this.props.cadenciaMedia}</p></div>
                <div><p>Calorias: {this.props.calorias}</p></div>
            </section>
        )
    }

}

class CyclingDaySummaryRefactor extends Component {
    render() {
        console.log(this.props)
        const{dia, distancia, maxHr, cadenciaMedia, calorias}=this.props
        return (
            <section>
                <div style={style}>Dia de ciclismo pero haciendo refactor al código</div>
                <div><p>Dia: {dia}</p></div>
                <div><p>Distancia: {distancia}</p></div>
                <div><p>Frecuencia cardiaca máxima: {maxHr}</p></div>
                <div><p>Cadencia media: {cadenciaMedia}</p></div>
                <div><p>Calorias: {calorias}</p></div>
            </section>
        )
    }

}
ReactDOM.render(
    <div>
        <Message msg="Como estás?" res="bien o que?" color="blue" minutes={5}/>
        <CyclingDaySummary dia={cyclingData.dia}
                           distancia={cyclingData.distancia}
                           maxHr={cyclingData.maxHr}
                           cadenciaMedia={cyclingData.cadenciaMedia}
                           calorias={cyclingData.calorias}/>
        <CyclingDaySummaryRefactor dia={cyclingData.dia}
                           distancia={cyclingData.distancia}
                           maxHr={cyclingData.maxHr}
                           cadenciaMedia={cyclingData.cadenciaMedia}
                           calorias={cyclingData.calorias}/>
    </div>, document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
