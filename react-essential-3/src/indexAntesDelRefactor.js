import React, {Component} from 'react'; // Import the react library
import ReactDOM from 'react-dom'; // Import the react dom
import './index.css'; //
import * as serviceWorker from './serviceWorker';

let bookList = [
    {"title": "El inversionista inteligente", "author": "Benjamin Graham", "pages": 792},
    {"title": "El cisne negro ", "author": "Nassim Nicholas Taleb", "pages": 592}
]

const Book = ({title, author, pages}) => {
    return (
        <section>
            <h2>{title}</h2>
            <p>{author}</p>
            <p>Pages: {pages}</p>
        </section>)
}

class Library extends Component {
    constructor(props) {
        super(props);
        //Aqui agregamos el estado al componente
        this.state = {
            open: true
        }

        // en el constructor debemos enlazar el metodo encargado de cambiar
        // el estado. 'this' hace accesible el metodo desde aquí en el constructor
        this.toggleOpenClosed = this.toggleOpenClosed.bind(this)
    }


    //Cambiando el estado
    toggleOpenClosed() {
        this.setState(previousState => ({
            open: !previousState.open
        }))
    }


    render() {
        console.log(this.state)
        // cuando definimos {books} hacemos que la propiedad props.book se enlace al {books}
        const {books} = this.props
        return (
            <div> Se debe utilizar un 'key' para identificar el recurso al que se accede
                <h1>La librería está {this.state.open ? 'abierta' : 'cerrada'}</h1>
                <button onClick={this.toggleOpenClosed}>Cambiar el estado</button>
                {books.map(
                    (book, i) =>
                        <Book key={i}
                              title={book.title}
                              author={book.author}
                              pages={book.pages}/>
                )}
            </div>
        );
    }
}

ReactDOM.render(
    <div>
        <Library books={bookList}/>
    </div>
    , document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
