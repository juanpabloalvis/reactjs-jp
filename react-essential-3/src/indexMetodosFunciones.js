import React, {Component} from 'react'; // Import the react library
import ReactDOM from 'react-dom'; // Import the react dom
import './index.css'; //
import * as serviceWorker from './serviceWorker';

let style = {
    backgroundColor: 'orange',
    color: 'gray',
    fontFamily: 'Arial'
}

let cyclingData = {
    dia: 12,
    distancia: 94.7,
    distanciaEsperada: 120,
    maxHr: 158,
    cadenciaMedia: 81,
    calorias: 1479
}

const getPercent = decimal => {
    return decimal * 100 + '%'
}

const calcGoalProgres = (total, goal) => {
    return getPercent(total / goal)
}

const CyclingDaySummary = ({dia, distancia, distanciaEsperada, maxHr, cadenciaMedia, calorias}) => {
    return (
        <section>
            <div style={style}>Dia de ciclismo. Otro refactor al código</div>
            <div><p>Dia: {dia}</p></div>
            <div><p>Distancia: {distancia}</p></div>
            <div><p>Distancia Esperada: {distanciaEsperada}</p></div>
            <div><p>Frecuencia cardiaca máxima: {maxHr}</p></div>
            <div><p>Cadencia media: {cadenciaMedia}</p></div>
            <div><p>Calorias: {calorias}</p></div>
            <div><p>CUMPLIMIENTO DISTANCIA: {calcGoalProgres(distancia, distanciaEsperada)}</p></div>
        </section>
    )
}

class Message extends Component {
    render() {
        console.log(this.props)
        return (
            <div>
                <h1>Hello every one 3</h1>
                <h2 style={{color: this.props.color}}>{this.props.msg}</h2>
                <div style={style}>
                    <p>Este es un parrafo con estilo </p>
                </div>
                <p>Regresare en {this.props.minutes} minutos.</p>
            </div>
        )
    }
}


ReactDOM.render(
    <div>
        <Message msg="Como estás?" res="bien o que?" color="blue" minutes={5}/>
        <CyclingDaySummary dia={cyclingData.dia}
                           distancia={cyclingData.distancia}
                           distanciaEsperada={cyclingData.distanciaEsperada}
                           maxHr={cyclingData.maxHr}
                           cadenciaMedia={cyclingData.cadenciaMedia}
                           calorias={cyclingData.calorias}/>
    </div>, document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
