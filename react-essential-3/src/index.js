import React, {Component} from 'react'; // Import the react library
import ReactDOM from 'react-dom'; // Import the react dom
import './index.css'; //
import * as serviceWorker from './serviceWorker';
import PropTypes from 'prop-types'

let bookList = [
    {"title": "El inversionista inteligente", "author": "Benjamin Graham", "pages": 792},
    {"title": "El cisne negro ", "author": "Nassim Nicholas Taleb", "pages": 592},
    {"title": "Scala for data science", "pages": 416}
]


const Hiring = () => <div><p>La librería está contratando. Vaya a www.librería.com/jobs para mas informacion</p></div>

const NoHiring = () => <div><p>La librería no está contratando. Vuelve pronto</p></div>
// Podemos poner valores por defecto al crear el componente
const Book = ({title = "Sin título", author = "Sin author", pages = 0, freeBookmark}) => {
    return (
        <section>
            <h2>{title}</h2>
            <p>{author}</p>
            <p>Pages: {pages}</p>
            <p>Free Bookmark today: {freeBookmark ? 'yes' : 'no!'}</p>
        </section>)
}


class Library extends Component {
    static defaultProps = {
        books: [
            {"title": "Scala for data science", "author": "Pascal Bugnion", "pages": 416}
        ]
    }
    state = {
        open: false,
        freeBookmark: true,
        hiring: false,
        data: [],
        loading: false
    }

    componentDidMount() {
        console.log("El componente fue montado")
        this.setState({loading: true})
        fetch('https://hplussport.com/api/products/order/price/sort/asc/qty/3')
            .then(data => data.json()) // la cambiamos a formato json
            .then(data => this.setState({data, loading: false})) //seteamos nuestro data con el valor que vienes desde la url
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("El componente fue actualizado")
    }

    toggleOpenClosed = () => {
        this.setState(previousState => ({
            open: !previousState.open
        }))
    }

    render() {
        console.log(this.state)
        // cuando definimos {books} hacemos que la propiedad props.book se enlace al {books}
        const {books} = this.props
        return (
            <div> Se debe utilizar un 'key' para identificar el recurso al que se accede
                <h1>La librería está {this.state.open ? 'abierta' : 'cerrada'}</h1>
                <button onClick={this.toggleOpenClosed}>Cambiar el estado(Importante)</button>
                {books.map(
                    (book, i) =>
                        <Book key={i}
                              title={book.title}
                              author={book.author}
                              pages={book.pages}
                            //Enviamos este estado al hijo, via props
                              freeBookmark={this.state.freeBookmark}/>
                )}
                {this.state.hiring ? <Hiring/> : <NoHiring/>}
                {
                    this.state.loading
                        ? "cargando..."
                        : <div>
                            <h3>Productos de la semana en la tienda</h3>
                            {this.state.data.map((product, i) => {
                                return (
                                    <div key={i}>
                                        <h2>{product.name}</h2>
                                        <img src={product.image} height={200} alt={product.name}/>
                                    </div>)
                            })}
                        </div>}
            </div>

        );
    }
}

class FavoriteColorForm extends React.Component {
    state = {value: ""}
    // e means event
    newColor = e => this.setState({value: e.target.value})
    submit = e => {
        console.log(`Nuevo color:  ${this.state.value}`)
        e.preventDefault() //evita enviar el formulario
    }

    render() {
        return (
            <form onSubmit={this.submit}>
                <label>Favorite Color(find in console.log):
                    <input type="color" onChange={this.newColor}/>
                </label>
                <button>Submit</button>
            </form>
        )
    }
}

// PropTypes permite que los datos que recibe cada componente, sean 'Tipados'
Library.propTypes = {
    // si no hay otro valor deberia haber un error
    books: PropTypes.array
}
Book.propTypes = {
    title: PropTypes.string,
    author: PropTypes.string,
    pages: PropTypes.number,
    freeBookmark: PropTypes.bool
}
ReactDOM.render(
    <div>

        {/* Podemos usar los valores por defecto así, <Library /> o proveer los valores*/}
        <Library books={bookList}/>
        <FavoriteColorForm/>
    </div>
    , document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
