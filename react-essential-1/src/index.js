import React from 'react'; // Import the react library
import ReactDOM from 'react-dom'; // Import the react dom
import './index.css'; //
import * as serviceWorker from './serviceWorker';

let style = {
    backgroundColor: 'orange',
    color: 'gray',
    fontFamily: 'Arial'
}

//Primero creamos el elemento html
const title = React.createElement(
    'h1',
    {id: 'title', className: 'header', style: style},
    'Hello World '
)
// Segundo lo presentamos en el DOM el componente que acabamos de crear
// El elemento root hace referencia al archivo react-essential-1/public/index.html:29
// ReactDOM.render(title, document.getElementById('root'));


// hacemos una modificaiòn y definimos un elemento tipo lista y le pasamos el elemento anterior
const list = React.createElement(
    'ul',
    {id: 'list', className: 'header'},
    React.createElement(
        'li',
        {},
        'item on our list'
    )
)

ReactDOM.render(
    <div>
        <div>{title}</div>
        <div>{list}</div>
    </div>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
