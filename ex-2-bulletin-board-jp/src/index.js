import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import BoardFather from './BoardFather'
import registerServiceWorker from './registerServiceWorker'

// para desarrollo se utiliza: 
//jp\jp-bulletin-board>npm start
ReactDOM.render(<BoardFather count={50} />, document.getElementById('root'));

// Una vez terminado el desarrollo se construye el proyecto, el cual crea una construccion optimizada, se crea la 
// carpeta build/* que contiene el JS minimizado, listo para produccion:
//$jp\jp-bulletin-board>npm run build

//Este proyecto tambien se puede utilizar como un serividor estatico, como un servicio, o un servicio http
// para esto se debe instalar el servicio "serve" con el siguiente comando:
//jp\jp-bulletin-board>$ npm install -g serve
// despues de instalado, llamamos:
//jp\jp-bulletin-board>$ serve -s build

registerServiceWorker();
