import React,{Component} from 'react'
import Note from './Note'
import FaPlus from 'react-icons/lib/fa/plus'

class Board extends Component{

	constructor(props){
		super(props)
		this.state = {
			notes:[]
			//Para pruebas se utiliza asi
			/*
			notes:[
				{
					id:0,
					note:"Juanillo"
				},
				{
					id:1,
					note:"Alvis"
				},
				{
					id:2,
					note:"Another note"
				}
			]*/
		}
		//debo enlazarlas
		this.eachNote = this.eachNote.bind(this)
		this.add = this.add.bind(this)
		this.nextId = this.nextId.bind(this)
		this.update = this.update.bind(this)
		this.remove = this.remove.bind(this)
	}

	componentWillMount(){
		var self = this
		if(this.props.count){
			fetch(`https://baconipsum.com/api/?type=all-meat&sentences=${this.props.count}`)
				.then(response =>response.json())
				.then(json => json[0].split('. ')
						.forEach(sentence => self.add(sentence
								.substring(0, 25))))
		}
	}

	add(text){
		//damos otro uso a esta funcion callback
		this.setState(prevState=>({
			notes: [
				//... tomamos todas la notas
				...prevState.notes, 
				{
					id: this.nextId(),
					note: text
				}
			]
		}))
	}

	nextId(){
		this.uniqueId = this.uniqueId || 0
		return this.uniqueId ++
	}
	update(newText, i){
		/*Esta funcion toma el estado anterior*/
		console.log("actualizando....", i, "-", newText)
		/*Sobreescribe la nota con el nuevo texto*/
		this.setState(prevState => ({
			notes : prevState.notes.map(
											
				note =>(note.id !== i)?note:{...note, note: newText}
				)
		}))
	}

	remove(id){
		console.log('Eliminando item ', id)
		//Callback function:
		this.setState(prevState=>({
			notes: prevState.notes.filter(note=> note.id !== id)
		}))
	}

	eachNote(note, i){
		return (
			<Note 
				key={note.id} 
				index={note.id} 
				onChange={this.update}
				onRemove={this.remove}>
				{note.note}
			</Note>
			)
	}
	render(){
		return (
			<div className="board">
				{this.state.notes.map(this.eachNote)}
				<button onClick={this.add.bind(null, "new Note")}><FaPlus /></button>
			</div>
			)
	}
}

export default Board